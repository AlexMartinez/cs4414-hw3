#ifndef FAT_H
#define FAT_H

//#include "item.h"
#include <vector>
#include <map>
#include <fstream>
#include <memory>
#include <iostream>
using namespace std;

#include "debug.h"

struct Item;

//This is the base class for the Fat16, Fat32, and Fat12 classes.
//It contains essential data for determining where critical sections of the volume are and
//what type of FAT is encoded.
class Fat {

 protected:
	Fat();
	virtual void loadRootDirectory();
	void loadItem(int); //This loads a particular value from the BPB
	unsigned int itemValue(unsigned int i); //This returns the already loaded value of a BPB field
	unsigned int readWordToInt(unsigned int fileOffset); //This endian-safely reads in a word
	unsigned int readDwordToInt(unsigned int fileOffset);//This endian-safely reads in a dword
	void writeIntAsWord(unsigned int fileOffset, unsigned int i); //Endian-safely writes a word
	void writeIntAsDword(unsigned int fileOffset, unsigned int i); //Endian-safely writes a dword
	
	//Reads "size" number of bytes into a string and removes trailing whitespace
	//This is designed to read the filename and file extension of a 32-byte file descriptions
	string readString(unsigned int fileOffset, unsigned int size, bool trim);
	//This gives the sector number of the first sector in cluster n
	unsigned int firstSectorOfDataCluster(unsigned int n);
	//This gives the byte offset of the start of cluster n
	unsigned int fatByteOffsetFromCluster(unsigned int n);
	//This gives the sector number of the start of the cluster n
	unsigned int fatSectorFromCluster(unsigned int n);
	//This gives the cluster number corresponding to this sector (not used anywhere)
	unsigned int fatClusterFromFirstSector(unsigned int firstSector);
	//This gives the byte offset locating the fat entry corresponding to cluster n
	unsigned int fatEntryOffsetFromCluster(unsigned int n);
	//This gives the byte offset locating the start of sector "sector"
	unsigned int offsetFromSector(unsigned int sector);
	//This was going to be used to read from the FAT table loaded into memory, but
	//my program just reads from the disk every time
	unsigned int firstFatTableEntryFileOffset;
	//This returns whether cluster "cluster" is free
	virtual bool clusterIsFree(unsigned int cluster);
	//This returns true if there are at least "numClusters" clusters available
	virtual bool clustersAvailable(unsigned int numClusters);
	//This returns the number of data clusters required to hold "fileSize" bytes
	virtual unsigned int requiredClusters(unsigned int fileSize);
	//This returns whether or not cluster "cluster" is the end of a cluster-chain
	virtual bool isEoc(unsigned int cluster);
	//Given cluster "prevCluster", this returns the next cluster in the linked list, assuming
	//that one exists
	virtual unsigned int nextClusterInChain(unsigned int prevCluster);
	//Starting at cluster "cluster", this returns the byte offset of a free 32-byte area for
	//a file description. It also sets (*locationCluster) equal to the cluster in which the
	//file description area resides
	virtual unsigned int findFreeFileLocation(unsigned int cluster,
											  unsigned int* locationCluster);
	//This returns whether the 32-byte file description area starting at offset "fileOffset"
	//is free
	virtual bool isFreeFileLocation(unsigned int fileOffset);
	//This returns the cluster number of the first free cluster
	virtual unsigned int findFreeCluster();
	//Given cluster number "endCluster", this finds a free cluster and then sets the FAT
	//entry for "endCluster" to refer to the new free cluster, and sets the FAT entry for the
	//new free cluster to be the end-of-chain value
	virtual unsigned int extendClusterChain(unsigned int endCluster);
	//This performs the data transfer for the cpin command where inFile refers to the external
	//file system's source file and "firstCluster" refers to the firstCluster of the destination
	//file in the FAT volume
	virtual void writeInData(ifstream& inFile, unsigned int fileSize, unsigned int firstCluster);
	//This was going to be used for the FAT Table stored in memory, but my program 
	//reads from the disk every time it access the FAT
	void loadFatTableSector(unsigned int n);
	//Same as above
	vector<char> fatTable;
	//Same as above
	void loadFatTable();
 private:
	//This ensures that the volume has the two signature FAT bytes
	bool checkSignature();
	//This calculates the endianness of the machine that is inspecting the FAT volume
	void setLocalEndianness();
	//This determines what type of FAT the base class instance had loaded
	int determineType();
	//This loads the BPB block into memory, which useful values such as FirstRootDirSecNum
	void loadHeader();
 public:
	//This factory function instantiates the appropriate subclass and returns a pointer to it
	static unique_ptr<Fat> open(char*);
	//This takes in a line of code supplied by the user and parses commands and arguments from it
	void processCommand(string command);
	//Runs the ls command with "path" as an argument
	virtual void ls(string path);
	//Runs the cd command with "path" as an argument
	virtual bool cd(string path);
	//Runs the cpout command with "fatPath" refering to the source file and "envPath" referring
	//to the destination file
	virtual bool cpout(string fatPath, string envPath);
	//Runs the cpin command with "envPath" referring to the source file and "fatPath"
	//referring to the destination file
	virtual bool cpin(string envPath, string fatPath);
	//Stores the endianness of the machine inspecting the FAT volume
	bool localBigEndian;
	//Gets the path of the current working directory
	string getCurrentPath();
	//An i/o file stream opened on the FAT volume
	unique_ptr<fstream> file;
	//The size, in bytes, of the FAT volume
	size_t fileSize;
	
	//This is a (stupid) inner class designed to enumerate and store some critical values from
	//the BPB block
	class Item {
		friend class Fat;
		friend class Fat16;
	public:
		std::vector<char> dataV;
		unsigned int offset;
		shared_ptr<fstream> file; //The FAT base class's file field is passed to this pointer
		Item() {
			
		}
	Item(unsigned int offsetSet, unsigned int size) {
			dataV.resize(size);
			offset = offsetSet;
		}

	};

	//Critical values calculated from the BPB Block Directly from the Microsoft Spec
	unsigned int RootDirSectors;
	unsigned int FirstRootDirSecNum;
	unsigned int FATSz;
	unsigned int FirstDataSector;
	unsigned int FirstSectorOfCluster;
	unsigned int CountOfClusters;
	unsigned int TotSec;
	unsigned int DataSec;

	enum ItemNum {
	    BS_jmpBoot,
		BS_OEMName,
		BPB_BytsPerSec,
		BPB_SecPerClus,
		BPB_RsvdSecCnt,
		BPB_NumFATs,
		BPB_RootEntCnt,
		BPB_TotSec16,
		BPB_Media,
		BPB_FATSz16,
		BPB_SecPerTrk,
		BPB_NumHeads,
		BPB_HiddSec,
		BPB_TotSec32,

		//Fat16 specific
		BS_DrvNum,
		BS_Reserved1,
		BS_BootSig,
		BS_VolID,
		BS_VolLab,
		BS_FilSysType
	};

	//This maps the enumerated value of a BPB element to an object containing its data
	map<int, Item> items;

	//This stores information about a File (including directories)
	struct File {
		File() {
			parentOffset = 0;
			fatOffset = 0;
			size = 0;
			firstCluster = 0;
			isSystem = false;
			isDirectory = false;
			isHidden = false;
			name = "";
			path = "";
		}
		File(const File* parent, unsigned int fatOffset_, bool isDirectory_,
			 bool isVolumeId_, bool isSystem_, string name_, unsigned int firstCluster_,
			 unsigned int size_) {
			fatOffset = fatOffset_;
			isDirectory = isDirectory_;
			isVolumeId = isVolumeId_;
			isSystem = isSystem_;
			name = name_;
			firstCluster = firstCluster_;
			size = size_;
			if (parent == nullptr) {
				path = "/";
				parentOffset = 0;
			}
			else {
				//if (this->name == ".") {
					
					//}
				if (name == ".") {
					path = parent->path;
				} else if (name == "..") {
					path = parent->path;
					int sepIndex = -1;
					for (int i = path.length() - 1; i >= 0; --i) {
						if (path[i] == '/') {
							sepIndex = i;
							break;
						}
					}
					
					if (sepIndex != -1) {
						path = path.substr(0, sepIndex);
					}
				}
				else if (parent->path == "/") {
					path += "/" + this->name;
				} else {
					path = parent->path + "/" + this->name;
				}
				parentOffset = parent->fatOffset;
			}
		}
		unsigned int fatOffset; //The offset of this File's file description
		bool isDirectory; //Some file attributes
		bool isVolumeId;
		bool isHidden;
		bool isSystem;
		string name; //The leaf name of this file
		string path; //The path of this file not including the leaf
		unsigned int size; //The size of this file, unless it is a directory
		unsigned int firstCluster; //The cluster number of this file's first data cluster
		vector<unsigned int> childrenOffset; //Byte offsets to file description's of this 
		                                     //file's children assuming this is a directory
		unsigned int parentOffset;//The byte offset of the parent directory's file description
	};

	File rootDir; //A File object encapsulating the root directory
	File currentDir; //A File object encapsulating the current working directory
	virtual void loadChildDirectories(File& parent); //This simply populates parent's
	                                                 //childrenOffset vector by searching 
	                                                 //data clusters starting with
	                                                 //parent.firstCluster

	
	//This goes to the byte offset "fileOffset" of the FAT volume and reads the 32 bytes
	//there into "destFile". A pointer to the parent is passed so that "destFile" has the
	//proper path set
	virtual int readFile(unsigned int fileOffset, File* parent, File& destFile);
	//This searches the current working directory for a file having name "fileName", and
	//if it exists, then it is read into "destFile"
	virtual bool readFileFromCwd(string fileName, File& destFile);
	//This goes to fileOffset and uses the 32 bytes there to wrie a file description
	//for the File encapsulated by "srcFile"
	virtual void writeFileDescription(unsigned int fileOffset, File& srcFile);
};

#endif
