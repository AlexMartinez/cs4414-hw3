#ifndef DEBUG_H
#define DEBUG_H

#define DEBUG

#include <iostream>
using namespace std;

#ifdef DEBUG
#define DEB(x) do { \
	auto Macro_tmp = (x); \
cout << #x << ": " << Macro_tmp << endl;								\
	} while(0)
#else
#define DEB(X) do {} while (0)
#endif


#endif
