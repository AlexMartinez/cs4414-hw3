#ifndef ITEM_H
#define ITEM_H

#include <vector>

struct Item {
	std::vector<char> data;
	int offset;

	Item(int offsetSet, int size) {
		data.resize(size);
		offset = offsetSet;
	}
};

#endif
