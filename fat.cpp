#include "fat.h"
#include "fat16.h"

#include "debug.h"
#include <stdio.h>
#include <sstream>

//This stores the offsets and sizes of some BPB values
Fat::Fat() : file(new fstream())
{
	items[BS_jmpBoot] = Item(0, 3);
	items[BS_OEMName] = Item(3, 8);
	items[BPB_BytsPerSec] = Item(11, 2);
	items[BPB_SecPerClus] = Item(13, 1);
	items[BPB_RsvdSecCnt] = Item(14, 2);
	items[BPB_NumFATs] = Item(16, 1);
	items[BPB_RootEntCnt] = Item(17, 2);
	items[BPB_TotSec16] = Item(19, 2);
	items[BPB_Media] = Item(21, 1);
	items[BPB_FATSz16] = Item(22, 2);
	items[BPB_SecPerTrk] = Item(24, 2);
	items[BPB_NumHeads] = Item(26, 2);
	items[BPB_HiddSec] = Item(28, 4);
	items[BPB_TotSec32] = Item(32, 4);
}

void Fat::loadRootDirectory() {
	//Stub here because Fat is not completely abstract, yet
	//this function is polymorphic
}

void Fat::loadChildDirectories(Fat::File& parent) {
	//Stub here because Fat is not completely abstract, yet
	//this function is polymorphic
}

int Fat::readFile(unsigned int fileOffset, Fat::File* parent, Fat::File& destFile) {
	//Stub here because Fat is not completely abstract, yet
	//this function is polymorphic

	return -1;
}

bool Fat::readFileFromCwd(string fileName, File& destFile) {
	//Polymorphic stub

	return false;
}

//Loads the enumerated BPB value from the FAT volume
void Fat::loadItem(int i) {
	Item& item = items[i];
	file->seekg(item.offset);
	file->read(item.dataV.data(), item.dataV.size());
}

//Trivial functions perform as described in fat.h

bool Fat::checkSignature() {
	char signature[2];
	file->seekg(510);
	file->read(signature, 2);
	return (signature[0] == (char)0x55 && (int)signature[1] == (char)0xAA);
}

//Used to automatically populate the items map with BPB values
void Fat::loadHeader() {
	for (auto iterator = items.begin(); iterator != items.end(); iterator++) {
	    loadItem(iterator->first);
	}
}

//This was going to be used to keep the FAT Table in memory, but was not completed
void Fat::loadFatTable() {
	fatTable.resize(CountOfClusters);
	int sector = fatSectorFromCluster(2);
	int fileOffset = offsetFromSector(sector);
	firstFatTableEntryFileOffset = fileOffset;
	
	file->seekg(fileOffset);
	file->read(fatTable.data(), FATSz);

	cout << endl;
}

//Same as above
void Fat::loadFatTableSector(unsigned int n) {
	

	// int fileOffset = 0; //TODO calculate
	// int tableOffset = fileOffset - firstFatTableEntryFileOffset;
	// int entryOffset = fatEntryOffsetFromCluster(2);

	//fatTable[tableOffset + entryOffset]
}

//This returns the byte offset in the FAT volume of the start of sector "sector"
unsigned int Fat::offsetFromSector(unsigned int sector) {
	return itemValue(BPB_BytsPerSec) * sector;
}

bool Fat::clusterIsFree(unsigned int cluster) {
	//Polymorphic
	return false;
}

bool Fat::clustersAvailable(unsigned int numClusters) {
	//Polymorphic
	return false;
}

unsigned int Fat::requiredClusters(unsigned int fileSize) {
	//Polymorphic
	return 0;
}

bool Fat::isEoc(unsigned int cluster) {
	//Polymorphic
	return false;
}

//Polymorphic, polymorphic, ...  for stubs
unsigned int Fat::nextClusterInChain(unsigned int prevCluster) {
	return 0xFFFFFFFF;
}

void Fat::writeFileDescription(unsigned int fileOffset, Fat::File& srcFile) {

}

void Fat::writeInData(ifstream& inFile, unsigned int fileSize, unsigned int firstCluster) {

}

unsigned int Fat::findFreeFileLocation(unsigned int cluster, unsigned int* locationCluster) {
	return 0xFFFFFFFF;
}

bool Fat::isFreeFileLocation(unsigned int fileOffset) {
	return false;
}

unsigned int Fat::findFreeCluster() {
	return 0;
}

//Grabs the least significant two bytes from "i" and writes them in the proper endian order
//into the FAT volume at byte offset "fileOffset"
void Fat::writeIntAsWord(unsigned int fileOffset, unsigned int i) {
	file->seekp(fileOffset);

	char word[2];

	union {
		unsigned int rawI;
		char rawC[4];
	};

	rawI = i;
	
	if (localBigEndian) {
		word[0] = rawC[3];
		word[1] = rawC[2];
	} else {
		word[0] = rawC[0];
		word[1] = rawC[1];
	}

	file->write(word, 2);
}

//Writes "i" to the FAT volume at byte-offset "fileOffset"in the proper byte-order
void Fat::writeIntAsDword(unsigned int fileOffset, unsigned int i) {
	file->seekp(fileOffset);
	
	char word[4];

	union {
		unsigned int rawI;
		char rawC[4];
	};

	rawI = i;

	if (localBigEndian) {
		word[0] = rawC[3];
		word[1] = rawC[2];
		word[2] = rawC[1];
		word[3] = rawC[0];
	} else {
		word[0] = rawC[0];
		word[1] = rawC[1];
		word[2] = rawC[2];
		word[3] = rawC[3];
	}

	file->write(word, 4);
}

//polymorphic
unsigned int Fat::extendClusterChain(unsigned int endCluster) {
	return 0;
}

//Used to find the byte offset for data cluster "n"
//Should have been polymorphic (there are more examples of functions that should shouldn't have
//been
unsigned int Fat::fatByteOffsetFromCluster(unsigned int n) {
	//TODO Different for FAT32
	unsigned int fatOffset = n * 2;
	return fatOffset % itemValue(BPB_BytsPerSec);
}

//Used to find the data sector number from cluster number "n"
unsigned int Fat::fatSectorFromCluster(unsigned int n) {
	unsigned int fatOffset = n * 2;
	return itemValue(BPB_RsvdSecCnt) + (fatOffset / itemValue(BPB_BytsPerSec));
}

//Not used in program
unsigned int Fat::fatClusterFromFirstSector(unsigned int firstSector) {
	unsigned int fatOffset = (firstSector - itemValue(BPB_RsvdSecCnt)) *
		itemValue(BPB_BytsPerSec);

	if (fatOffset & 1) //if odd
		++fatOffset;
	
	return fatOffset / 2;
}

//This finds the byteoffset of the FAT Table entry corresponding to data cluster "n"
unsigned int Fat::fatEntryOffsetFromCluster(unsigned int n) {
	return offsetFromSector(fatSectorFromCluster(n)) +
		fatByteOffsetFromCluster(n);
}

//This finds the sector number of the first sector in cluster "n"
unsigned int Fat::firstSectorOfDataCluster(unsigned int n) {
	return ((n - 2) * itemValue(BPB_SecPerClus)) + FirstDataSector;
}

//This converts the raw bytes of a BPB value into an unsigned integer
unsigned int Fat::itemValue(unsigned int i) {
	union {
		unsigned int retI;
		char c[4];
	};

	Item& item = items[i];
	
	for (unsigned int i = 0; i < 4; ++i) {
		unsigned int index = localBigEndian ? 3 - i : i;
		
		if (i < item.dataV.size()) {
			c[index] = item.dataV[index];
		} else {
			c[index] = 0;
		}
	}
	
	return retI;
}

//This reads a word value from the FAT volume at byte-offset "fileOffset" into an integer
unsigned int Fat::readWordToInt(unsigned int fileOffset) {
	char word[2];
	file->seekg(fileOffset);
	file->read(word, 2);

	union {
		unsigned short retS;
		char c[2];
	};

	if (localBigEndian) {
		c[1] = word[0];
		c[0] = word[1];
	} else {
		c[0] = word[0];
		c[1] = word[1];
	}
	return (unsigned int) retS;
}

//This reads a word value from the FAT volume at byte-offset "fileOffset" into an integer
unsigned int Fat::readDwordToInt(unsigned int fileOffset) {
	union {
		unsigned int retI;
		char c[4];
	};

	char dword[4];
	file->seekg(fileOffset);
	file->read(dword, 4);

	for (int i = 0; i < 4; ++i) {
		int cIndex = localBigEndian ? 3 - i : i;

		c[cIndex] = dword[i];
	}

	return retI;
}

string Fat::readString(unsigned int fileOffset, unsigned int size, bool trim) {
	vector<char> stringV(size + 1, 0);
	file->seekg(fileOffset);
	file->read(stringV.data(), size);

	if (trim) {
		for (int i = size -1; i >= 0; --i) {
			if (stringV[i] == ' ')
				stringV[i] = 0;
			else
				break;
		}
	}

	return string(stringV.data());
}

int Fat::determineType() {
	file->seekg(0, file->end);
	fileSize = file->tellg();
	if (fileSize < 512) { //BPB block has a minimum size of 512
		cerr << "File is not a FAT file" << endl;
		return -1;
	}
    
	loadHeader();

	RootDirSectors = ((itemValue(BPB_RootEntCnt) * 32) + (itemValue(BPB_BytsPerSec) - 1)) /
    	itemValue(BPB_BytsPerSec);

	if (itemValue(BPB_FATSz16) != 0) {
	 	FATSz = itemValue(BPB_FATSz16);
	} else {
		//TODO
		//FATSz = BPB_FATSz32.intValue();
	}

	//The sector number of the first non-reserved data cluster
	FirstDataSector = itemValue(BPB_RsvdSecCnt) + (itemValue(BPB_NumFATs) * FATSz) +
	 	RootDirSectors;

	if (itemValue(BPB_TotSec16) != 0) {
		TotSec = itemValue(BPB_TotSec16);
	} else {
		TotSec = itemValue(BPB_TotSec32);
	}

	DataSec = TotSec - (itemValue(BPB_RsvdSecCnt) + (itemValue(BPB_NumFATs) * FATSz) +
						RootDirSectors);

	CountOfClusters = DataSec / itemValue(BPB_SecPerClus);

	if (CountOfClusters < 4085) {
		//Fat12
		return 0;
	} else if (CountOfClusters < 65525) {
		//Fat16
		return 1;
	} else {
		//Fat32
		return 2;
	}
	return 0;
}

void Fat::setLocalEndianness() {
	union {
		uint32_t i;
		char c[4];
	} test;
	test.i = 0x01020304;
	localBigEndian = test.c[0] == 1;
}

unique_ptr<Fat> Fat::open(char* fileName) {
	unique_ptr<Fat> pFat(new Fat);
	pFat->setLocalEndianness();

	pFat->file->open(fileName, fstream::in | fstream::out | fstream::binary);
	if (!pFat->file->good()) {
		//TODO
		cerr << "Failure to open file" << endl;
		return 0;
	}
   
	if (!pFat->checkSignature()) {
		//TODO
		cerr << "Incorrect FAT signature" << endl;
		return 0;
	}

	int fatType = pFat->determineType();

	unique_ptr<Fat> ret;
	switch (fatType) {
	case 0: //Fat12
		break;
	case 1://Fat16
		ret.reset(new Fat16(*pFat));
		break;
	case 2: //Fat32
		break;
	};

	if (ret != nullptr) {
		ret->loadRootDirectory();
		ret->currentDir = ret->rootDir;
		ret->loadFatTable(); //Not used
	}

	return ret;
}

string Fat::getCurrentPath() {
	return currentDir.path;
}

void Fat::processCommand(string command) {
	stringstream str(command);
	string token;
	str >> token; //Grabs the first white-space delimited token
	
	if (token == "ls") {
		token = "";
		str >> token; //path argument
		ls(token);
	}
	else if (token == "cd") {
		token = "";
		str >> token; //path argument
		cd(token);
	}
	else if (token == "cpout") {
		string src = "", dest = "";
		str >> src; // src path
		str >> dest;// dest path
		cpout(src, dest);
	}
	else if (token == "cpin") {
		string src = "", dest = "";
		str >> src; //src path
		str >> dest;// dest path
		cpin(src, dest);
	}
	else {
		cout << "Unrecognized command: " << command << endl;
	}
}

//Polymorphic ahead
void Fat::ls(string paths) {

}

bool Fat::cd(string path) {
	return false;
}

bool Fat::cpout(string fatPath, string envPath) {
	return false;
}

bool Fat::cpin(string envPath, string fatPath) {
	return false;
}
