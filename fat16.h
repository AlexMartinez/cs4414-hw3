#ifndef FAT16_H
#define FAT16_H

#include "fat.h"

class Fat16 : public Fat {
 public:
	Fat16();

	//This is used to transfer data from the temporary Fat object created in Fat::open
	//to the dynamically created Fat16 object
	Fat16(Fat&);
 protected:
	//*** All virtual methods perform as described in fat.h

	virtual bool clusterIsFree(unsigned int cluster);
	virtual bool clustersAvailable(unsigned int numClusters);
	virtual unsigned int requiredClusters(unsigned int fileSize);
	virtual bool isEoc(unsigned int cluster);
	virtual unsigned int nextClusterInChain(unsigned int prevCluster);
	virtual unsigned int findFreeCluster();
	virtual unsigned int extendClusterChain(unsigned int endCluster);
	virtual unsigned int findFreeFileLocation(unsigned int cluster,
											  unsigned int* locationCluster);
	virtual bool isFreeFileLocation(unsigned int fileOffset);
	virtual void writeFileDescription(unsigned int fileOffset, File& srcFile);
	virtual void writeInData(ifstream& inFile, unsigned int fileSize, unsigned int firstCluster);
	virtual void loadRootDirectory();
	virtual void loadChildDirectories(File& parent);
	virtual int readFile(unsigned int fileOffset, File* parent, File& destFile);
	virtual bool readFileFromCwd(string fileName, File& destFile);
	virtual void ls(string path);
	virtual bool cd(string path);
	virtual bool cpout(string fatPath, string envPath);
	virtual bool cpin(string envPath, string fatPath);
};

#endif
