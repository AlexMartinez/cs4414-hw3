#include "fat16.h"

#include <sstream>
#include <fstream>
#include <algorithm>

Fat16::Fat16(Fat& fat)
{
	//Load Fat16 specific BPB values
	items[BS_DrvNum] = Item(36, 1);
	items[BS_Reserved1] = Item(37, 1);
	items[BS_BootSig] = Item(38, 1);
	items[BS_VolID] = Item(39, 4);
	items[BS_VolLab] = Item(43, 11);
	items[BS_FilSysType] = Item(54, 8);

	//Copy over information from the original base class instance
	for (auto iterator = fat.items.begin(); iterator != fat.items.end(); iterator++) {
		items[iterator->first] = iterator->second;
	}

	file = move(fat.file);

	RootDirSectors = fat.RootDirSectors;
	FATSz = fat.FATSz;
	FirstDataSector = fat.FirstDataSector;
	FirstSectorOfCluster = fat.FirstSectorOfCluster;
	CountOfClusters = fat.CountOfClusters;
	TotSec = fat.TotSec;
	DataSec = fat.DataSec;

	//Specific to FAT16

	FirstRootDirSecNum = itemValue(BPB_RsvdSecCnt) + (itemValue(BPB_NumFATs) 
													  * itemValue(BPB_FATSz16));
}

bool Fat16::clusterIsFree(unsigned int cluster) {
	unsigned int offset = fatEntryOffsetFromCluster(cluster);
	file->seekg(offset);
	char allocated = file->get();
	return allocated == (char) 0;
}

bool Fat16::clustersAvailable(unsigned int numClusters) {
	for (unsigned int cluster = 2; cluster < CountOfClusters + 2; ++cluster) {
		if (clusterIsFree(cluster))
			--numClusters;

		if (numClusters == 0)
			return true;
	}

	return false;
}

unsigned int Fat16::extendClusterChain(unsigned int endCluster) {
	unsigned int freeCluster = findFreeCluster();
	if (freeCluster == 0) { //could not find free cluster
		return 0;
	}

	unsigned int endChainLocation = fatEntryOffsetFromCluster(endCluster);
	writeIntAsWord(endChainLocation, freeCluster);

	unsigned int newClusterLocation = fatEntryOffsetFromCluster(freeCluster);
	writeIntAsWord(newClusterLocation, 0xFFFF);

	return freeCluster;
}

unsigned int Fat16::findFreeCluster() {
	for (unsigned int cluster = 2; cluster < CountOfClusters + 2; ++cluster) {
		if (clusterIsFree(cluster))
			return cluster;
	}

	return 0;
}

unsigned int Fat16::requiredClusters(unsigned int fileSize) {
	unsigned int clusterSize = offsetFromSector(firstSectorOfDataCluster(3)) -
		offsetFromSector(firstSectorOfDataCluster(2));

    unsigned int numClusters = fileSize / clusterSize;
	if ((fileSize % clusterSize) != 0)
		++numClusters;

	return numClusters;
}

bool Fat16::isEoc(unsigned int cluster) {
	unsigned int fatLocation = fatEntryOffsetFromCluster(cluster);
	unsigned int nextCluster = readWordToInt(fatLocation);
	return nextCluster >= 0xFFF8;
}

unsigned int Fat16::nextClusterInChain(unsigned int prevCluster) {
	unsigned int fatLocation = fatEntryOffsetFromCluster(prevCluster);
	unsigned int nextCluster = readWordToInt(fatLocation);
	return nextCluster;
}

unsigned int Fat16::findFreeFileLocation(unsigned int cluster, unsigned int* locationCluster) {

	while (true) {
		//First sector of the current cluster
		unsigned int firstSector = firstSectorOfDataCluster(cluster);
		if (currentDir.fatOffset == rootDir.fatOffset) { //If currently in root directory
			//Then use a different value for the first sector
			firstSector = FirstRootDirSecNum;
		}

		//The first sector of the next cluster
		unsigned int nextClusterSector = firstSectorOfDataCluster(cluster + 1);
		if (currentDir.fatOffset == rootDir.fatOffset) {
			nextClusterSector = firstSector + RootDirSectors;
		}
	
		//Iterate through the sectors in this cluster
		for (unsigned int currentSector = firstSector; currentSector < nextClusterSector;
			 ++currentSector) {

			unsigned int sectorStartOffset = offsetFromSector(currentSector);
			unsigned int nextSectorOffset = offsetFromSector(currentSector + 1);

			//Iterate through 32 bytes file description areas in this sector
			for (unsigned int currentOffset = sectorStartOffset; currentOffset < nextSectorOffset;
				 currentOffset += 32) {

				//Check if "currentOffset" can be filled with a new file description
				if (isFreeFileLocation(currentOffset)) {
					if (locationCluster != nullptr) {
						(*locationCluster) = cluster;
					}
					return currentOffset;
				}
			}
		}

		//If this is the root directory, then do not look beyond this cluster for more directory
		//space
		if (currentDir.fatOffset == rootDir.fatOffset) {
			return 0xFFFFFFFF;
		}

		//If this cluster is the last one belonging to the file being searched
		if (isEoc(cluster))
			break;

		//Search FAT table for the next cluster in this chain for free file space
		cluster = nextClusterInChain(cluster);
	}

	if (locationCluster != nullptr) {
		//When the last
		(*locationCluster) = cluster; //Allows retrieval of last cluster in chain
		                              //So that chain can be extended when no free file space is
		//                            found in the current cluster chain
	}
	return 0xFFFFFFF; //Need to allocate a new cluster for this file (unless parent is root)
}

bool Fat16::isFreeFileLocation(unsigned int fileOffset) {
	file->seekg(fileOffset);
	char allocated = (char) file->get();
	return allocated == 0;
}

int Fat16::readFile(unsigned int offset, Fat::File* parent, Fat::File& destFile) {
	file->seekg(offset);

	char allocated = (char) file->get();
	if (allocated == (char)0xE5)
		return 0; //this spot deallocated, check next
	else if (allocated == (char)0x00)
		return -1;//no files remaining in sector
	
	//Get ready to grab file attributes
	file->seekg(offset + 11);
	char attr = (char) file->get();

	//If this is a long directory, the ignore it
	if ((attr & 0x01) && //ATTR_READO_ONLY
		(attr & 0x02) && //ATTR_HIDDEN
		(attr & 0x04) && //ATTR_SYSTEM
		(attr & 0x08)) { //ATTR_VOLUME_ID
		//TODO Long directory
		return 0; //skip to find correspondin short-name file description
	}
	
	bool isDir = attr & 0x10;
	bool isVolumeId = attr & 0x08;
	bool isSystem = attr & 0x04;
	
	string name = readString(offset, 8, true);
	if (name[0] == (char)0x05) //Japanese Kanji
		name[0] = (char)0xE5; //Normally this would represent a deallocated file description
	//block
	
	string extensionStr = readString(offset + 8, 3, true);
	
	if (extensionStr[0] != 0) {
		name += "." + extensionStr;
	}
	
	//The first data cluster of this
	unsigned int cluster = readWordToInt(offset + 26);
	unsigned int size = readDwordToInt(offset + 28);
	
	destFile = File(parent, offset, isDir, isVolumeId, isSystem, name, cluster, size);

	return 1;
}

bool Fat16::readFileFromCwd(string fileName, Fat::File& destFile) {
	File child;
	bool found = false;
	for (unsigned int offset : currentDir.childrenOffset) {
		//Read child file descriptions until a child with matching name is found
		if (readFile(offset, &currentDir, child) == 1) {
			if (child.name == fileName) {
				found = true;
				break;
			}
		}
	}

	if (found) {
		destFile = child; //If the file was found, then it is passed to the reference
		//parameter
		return true;
	} else {
		return false;
	}
}

void Fat16::writeInData(ifstream& inFile, unsigned int fileSize, unsigned int firstCluster) {
	inFile.seekg(0);

	unsigned int firstFat = fatEntryOffsetFromCluster(firstCluster);
	writeIntAsWord(firstFat, 0xFFFF); //Mark first as end of chain in case the file entirely
	//fits in this cluster

	unsigned int bytesRemaining = fileSize;
	const unsigned int bufferSize = 8192;
	char transferBuffer[bufferSize]; //Transfers between external  fileystem and the FAT volume
	// a fixed size buffer so that memory is not consumed
	unsigned int cluster = firstCluster;

	//Iterating through clusters until the file has been copied entirely in
	while (true) {
		unsigned int sector = firstSectorOfDataCluster(cluster);
		unsigned int nextClusterSector = firstSectorOfDataCluster(cluster + 1);
		unsigned int currentOffset = offsetFromSector(sector);
		unsigned int nextOffset = offsetFromSector(nextClusterSector);
	    unsigned int clusterSize = nextOffset - currentOffset;
		unsigned int clusterRemaining = clusterSize;

		while (clusterRemaining > 0) {
			//Determine the read size from the minimum of remaining cluster bytes, the buffer size, and the remaining number of bytes to be read from the external file
			unsigned int readSize = clusterRemaining > bufferSize ? bufferSize : clusterRemaining;
			if (bytesRemaining < readSize)
				readSize = bytesRemaining;

			file->seekp(currentOffset + (clusterSize - clusterRemaining));
			inFile.read(transferBuffer, readSize);
			file->write(transferBuffer, readSize);
			clusterRemaining -= readSize;
			bytesRemaining -= readSize;

			if (bytesRemaining == 0) {
			    goto READ_COMPLETE;
			}
		}

		//If the end of a cluster was reached without exhausting bytesRemaining, then
		//extend the cluster chain.
		//This is guaranteed to work because clustersAvailable(unsigned int numClusters)
		//is called before this in the cpin function
		cluster = extendClusterChain(cluster);
	} READ_COMPLETE:

	return;
}

void Fat16::writeFileDescription(unsigned int fileOffset, Fat::File& srcFile) {
    char fileName[11];

	//Convert the char array into space-trailed strengths of length 8 and 3 split upon the first
	//dot character
	int dotIndex = -1;
	for (int i = 0; i < srcFile.name.length(); ++i) {
		if (srcFile.name[i] == '.') {
			dotIndex = i;
			break;
		}
	}
	
	string name = "", ext = "";

	//If dot character was encountered
	if (dotIndex == -1) {
		name = srcFile.name;
	} else {
		name = srcFile.name.substr(0, dotIndex);
		if (dotIndex + 1 < srcFile.name.length()) {
			ext = srcFile.name.substr(dotIndex + 1);
		}
	    
	}

	//load 8-char file name
	for (int i = 0; i < 8; ++i) {
		if (i < name.length()) {
			fileName[i] = name[i];
		} else {
			fileName[i] = ' ';
		}
	}

	//load 3-char file extension
	for (int i = 8; i < 11; ++i) {
		int extIndex = i - 8;
		if (extIndex < ext.length()) {
			fileName[i] = ext[extIndex];
		} else {
			fileName[i] = ' ';
		}
	}

	//Write part of file description into data cluster of parent directory
	file->seekp(fileOffset);
	file->write(fileName, 11);

	char zero[15] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	file->write(zero, 15);
	
	writeIntAsWord(fileOffset + 26, srcFile.firstCluster);
	writeIntAsDword(fileOffset + 28, srcFile.size);
}

void Fat16::loadRootDirectory() {
	rootDir = File(nullptr, offsetFromSector(FirstRootDirSecNum),
				   true, false, false, "", fatClusterFromFirstSector(FirstRootDirSecNum), 0);


	//Iterate through all sectors of the root directory
 	for (unsigned int sectorCount = 0; sectorCount < RootDirSectors; ++sectorCount) {
 		unsigned int sector = FirstRootDirSecNum + sectorCount;
 		unsigned int offsetThisSector = offsetFromSector(sector);
 		unsigned int offsetNextSector = offsetFromSector(sector + 1);
		//Iterate through 32-byte blocks of memory
 		for (unsigned int offset = offsetThisSector; offset < offsetNextSector; offset += 32) {
 			file->seekg(offset);
 			char allocated = (char) file->get();
			//This means that this block was deallocated, but there might be an allocated block
			//ahead
 			if (allocated == (char)0xE5)
 				continue;
 			else if (allocated == (char)0x00)
 				goto SECTOR_BREAK;
			
			//Attributes described in readFile function
 			file->seekg(offset + 11);
 			char attr = (char) file->get();
 			if ((attr & 0x01) &&
 				(attr & 0x02) &&
 				(attr & 0x04) &&
 				(attr & 0x08)) {
 				//TODO Long directory
 				continue;
 			}

 			bool isDir = attr & 0x10;
 			bool isVolumeId = attr & 0x08;
			bool isSystem = attr & 0x04;

 			vector<char> fileName(8, 0);
 			file->seekg(offset);
 			file->read(fileName.data(), 8);
 			if (fileName[0] == (char)0x05)
 				fileName[0] = (char)0xE5;

 			vector<char> extension(3, 0);
 			file->seekg(offset + 8);
 			file->read(extension.data(), 3);

 			for (int i = 7; i >= 0; --i) {
 				if (fileName[i] == ' ') {
 					fileName[i] = (char) 0;
 				} else {
 					break;
 				}
 			}

 			for (int i = 2; i >= 0; --i) {
 				if (extension[i] == ' ') {
 					extension[i] = (char) 0;
 				} else {
 					break;
 				}
 			}

 			string name = string(fileName.data(), fileName.size());
 			string extensionStr = string(extension.data(), extension.size());

 			if (extensionStr[0] != 0) {
 				name += "." + string(extension.data(), extension.size());
 			}

 			unsigned int cluster = readWordToInt(offset + 26);
 			unsigned int size = readDwordToInt(offset + 28);

 			rootDir.childrenOffset.push_back(offset);
 		}
 	}
	
 SECTOR_BREAK:
 	return;
}

void Fat16::loadChildDirectories(Fat::File& parent) {

	if (!parent.isDirectory) //should probably throw exception
		return;

	//iterator sectors in this cluster
	//     for each sector: iterate 32 byte chunks for files

	//check FAT table to see if another cluster. if there is, then repeat above

	//NOTE: can put above in do{} while(another cluster in FAT linked list);

	//If there is another cluster of data in this chain
	bool anotherClusterExists = false;
	unsigned int cluster = parent.firstCluster;
	do {
		unsigned int sector = firstSectorOfDataCluster(cluster);
		unsigned int nextClusterSector = firstSectorOfDataCluster(cluster + 1);

		for (unsigned int currentSector = sector; currentSector < nextClusterSector;
			 ++currentSector) {

			unsigned int offset = offsetFromSector(currentSector);
			unsigned int nextSectorOffset = offsetFromSector(currentSector + 1);

			for (unsigned int currentOffset = offset; currentOffset < nextSectorOffset;
				 currentOffset += 32) {
				//TODO load file description and create file object***

				File child;
				//Read the 32 bytes starting at offset "currentOffset" into the "child"
				//File object, having parent "parent"
				int readResult = readFile(currentOffset, &parent, child);
				switch (readResult) {
				case -1: //There are no more file descriptions in this cluster
					goto BREAK_SECTOR;
				case 0:
					//There may be another file description in this cluster even though
					//this one was deallocated
					continue;
				case 1:
					//A valid file description is here, store its byte-offset in the parent's
					//vector of children offsets
					parent.childrenOffset.push_back(child.fatOffset);
					break;
				};
			}
		} BREAK_SECTOR:

		//check for another cluster by consulting the FAT entry for the current cluster
		unsigned int fatLocation = fatEntryOffsetFromCluster(parent.firstCluster);
		unsigned int nextCluster = readWordToInt(fatLocation);
		anotherClusterExists = nextCluster < 0xFFF8;
		cluster = nextCluster;

	} while (anotherClusterExists);
}

void Fat16::ls(string path) {
	File originalDir = currentDir; //Save the original file to cd back into after performing ls
	if (path != "") {
		if (!cd(path)) //Failed to cd there, maybe file not found
			return;
	}

	//Search all children of this directory and print out their names along with 
	//file attribute information
	for (unsigned int childOffset : currentDir.childrenOffset) {
		File child;
		//Need to reread this child's file description to reobtain the attributes
		readFile(childOffset, &currentDir, child);
		if (child.isDirectory) {
			cout << "D  ";
		} else if (child.isHidden) {
			cout << "H  ";
		} else if (child.isVolumeId) {
			cout << "Volume Label: ";
		} else if (child.isSystem) {
			cout << "S ";
		}
		cout << child.name << endl;
	}

	//go back to the original directory from which the ls command was issued
	currentDir = originalDir;
}

//Change the current directory to be the one referred to by "path"
bool Fat16::cd(string path) {
	if (path.length() == 0) //Maintain the same cwd
		return true;

	if (path[0] == '/') { //If this is an absolute path
		if (path == "/") {
			currentDir = rootDir; //Root directory as destination
			return true;
		}

		File originalDir = currentDir; //Store in case of failure
		currentDir = rootDir;
	    stringstream pathStr(path);
		string directory;
		getline(pathStr, directory, '/'); //remove root
		//If the argument is a path through multiple directories, then recursively cd into each
		//one step at a time
		while (getline(pathStr, directory, '/')) {
		    if (!cd(directory)) {
				currentDir = originalDir; //If failure occurs at any step, reset cwd to the
				//                          original path
				return false;
			}
		}
		return true;
	} else if (path.find('/') != string::npos) { //If the argument is a relative path
		stringstream pathStr(path);
		string directory;
		File originalDir = currentDir;
		while (getline(pathStr, directory, '/')) { //Recursively step through directory one-by-one
			if (!cd(directory)) {
				//If fail at any step, reset cwd
				currentDir = originalDir;
				return false;
			}
		}
		return true;
	}

	//This is the recursive base case handling an argument that is a simple file name and not
	//a path
	for (unsigned int childOffset : currentDir.childrenOffset) {
		File child;
		readFile(childOffset, &currentDir, child); //Search all children of the cwd for
		if (child.isDirectory) {                   //the directory with the same name as the
			if (child.name == path) {              //argument
				if (path == "..") {  //When using ".." to cd into root directory, just load
					                 //the already stored File object 
					if (child.firstCluster == 0) {
						currentDir = rootDir;
						return true;
					}
				}

				//Otherwise, load the children of the new cwd
				loadChildDirectories(child);
				currentDir = child;
				return true;
			}
		}
	}

	cout << "Directory does not exist" << endl;
	return false;
}

bool Fat16::cpout(string fatPath, string envPath) {
	if (fatPath == "" || envPath == "")
		//Path omitted
		return false;

	if (fatPath[fatPath.length() - 1] == '/' || envPath[envPath.length() - 1] == '/') {
		return false; //Can't copy directory
	}

    int fileIndex = -1;
	//This checks for the presence of the '/' character
	for (int it = fatPath.length() - 1; it >= 0; --it) {
		if (fatPath[it] == '/') {
			fileIndex = it + 1;
			break;
		}
	}

	File originalDir = currentDir; //Saves the original directory in case of command failure due
	                               //to invalid arguments
	string fileName;

	if (fileIndex != -1) {
		//if the supplied "fatPath" is not a simple file name, then split it into a path
		//and leaf
		string path = fatPath.substr(0, fileIndex);
	    fileName = fatPath.substr(fileIndex);

		if (!cd(path)) { //set cwd to the path of the source file
			return false;
		}
	} else {
		fileName = fatPath; //The argument is a simple file name
	}

    File srcFile;
	if (!readFileFromCwd(fileName, srcFile)) { //Find child of cwd with name of argument
		cout << "Could not open " << fileName << endl; //File not found
		currentDir = originalDir;
		return false;
	}

	ofstream outFile(envPath, ofstream::out | ofstream::binary);

    if (!outFile.good()) { //Make sure output file in external file system is opened
		cout << "Could not open file " << envPath << endl;
		currentDir = originalDir; //Failure, reset the cwd
		return false;
	}

	//Perform the data transfer
	unsigned int bytesRemaining = srcFile.size;
	const unsigned int bufferSize = 8192;
	char transferBuffer[bufferSize];

	bool anotherClusterExists = false;
	unsigned int cluster = srcFile.firstCluster;
	do {
		unsigned int sector = firstSectorOfDataCluster(cluster);
		unsigned int nextClusterSector = firstSectorOfDataCluster(cluster + 1);
		unsigned int currentOffset = offsetFromSector(sector);
		unsigned int nextOffset = offsetFromSector(nextClusterSector);
	    unsigned int clusterSize = nextOffset - currentOffset;
		unsigned int clusterRemaining = clusterSize;
	   
		while (clusterRemaining > 0) {
			//Read the minimum of 8192, the remaining bytes of cluster, and the remaining
			//bytes of the file
			unsigned int readSize = clusterRemaining > bufferSize ? bufferSize : clusterRemaining;
			if (bytesRemaining < readSize)
				readSize = bytesRemaining;

			//able to handle the case when the buffer size is smaller than the cluster size, and
			//the file size is larger than the buffer size
			file->seekg(currentOffset + (clusterSize - clusterRemaining));
			file->read(transferBuffer, readSize);
			outFile.write(transferBuffer, readSize);
			clusterRemaining -= readSize;
			bytesRemaining -= readSize;

			if (bytesRemaining == 0)
				goto READ_COMPLETE;
		}
		//check for another cluster
		unsigned int fatLocation = fatEntryOffsetFromCluster(cluster);
		unsigned int nextCluster = readWordToInt(fatLocation);
		//anotherClusterExists = nextCluster != 0xFFFF; //TODO change because of bad flag
		cluster = nextCluster;

	} while (true);
 READ_COMPLETE:
	
	currentDir = originalDir; //after successful copying outward, reset cwd
	return true;
}

bool Fat16::cpin(string envPath, string fatPath) {
	//Read external file size
	//Make sure there are enough free data clusters
	//        case1: if root directory, find free 32-byte section
	//        case2: if not in root directory, then iterate to end of cluster pointed to by
	//                parent directory and if no free 32 byte section, then allocate a new cluster
	//                and fill that new cluster

	//make sure there are enough free clusters for the actual file data

	if (fatPath == "" || envPath == "")
		//Path omitted
		return false;

	if (envPath[envPath.length() - 1] == '/' || fatPath[fatPath.length() - 1] == '/') {
		return false; //Can't copy directory
	}

    int fileIndex = -1;
	for (int it = fatPath.length() - 1; it >= 0; --it) {
		if (fatPath[it] == '/') {
			fileIndex = it + 1;
			break;
		}
	}

	File originalDir = currentDir;
	string fileName;

	if (fileIndex != -1) {
		string path = fatPath.substr(0, fileIndex);
	    fileName = fatPath.substr(fileIndex);

		if (!cd(path)) {
			return false;
		}
	} else {
		fileName = fatPath;
	}

	//check file name
	//make sure the supplied filename is valid based on the FAT spec
	int beforeDot = 0, afterDot = 0;
	bool reached = false;
	for (int i = 0; i < fileName.length(); ++i) {
		if (!reached) {
			if (fileName[i] == '.') {
				reached = true;
				continue;
			}
		}

		if (reached) {
			++afterDot; //count number of characters to left of first dot
		} else {
			++beforeDot; //count number of characters to right of first dot
		}
	}

	if (beforeDot > 8 || afterDot > 3) {
		cout << "Invalid file name: " << fileName << endl;
		currentDir = originalDir; //reset cwd to original state
		return false;
	}

	File child;
	if (readFileFromCwd(fileName, child)) { //Check if dest file already exists in FAT volume
		cout << "That file already exists" << endl;
		currentDir = originalDir;
		return false;
	}

	ifstream inFile(envPath, ios::in | ios::binary | ios::ate);
	if (!inFile.good()) {
		cout << "Could not open " << envPath << endl;
		currentDir = originalDir;
		return false;
	}

	unsigned int fileSize = inFile.tellg();
	unsigned int numRequiredClusters = requiredClusters(fileSize);
	unsigned int locationCluster;
	unsigned int fileOffset;	

	fileOffset = findFreeFileLocation(currentDir.firstCluster, &locationCluster);
   

	if (fileOffset == 0xFFFFFFFF) { //Check if another cluster should be allocated for directory
		                            //entry
		if (currentDir.fatOffset == rootDir.fatOffset) {
			cout << "Not enough space in root directory for another file" << endl;
			currentDir = originalDir;
			return false;
		}
		++numRequiredClusters;
	}

	if (!clustersAvailable(numRequiredClusters)) {
		//Check if total number of required data clusters are available before modifying
		//the FAT volume
		cout << "Not enough space for file " << endl;
		return false;
	}

	//Now we have enough room for additional directory cluster and all data clusters

	if (fileOffset == 0xFFFFFFFF) { //Need to extend directory cluster chain
		locationCluster = extendClusterChain(locationCluster);
		fileOffset = offsetFromSector(firstSectorOfDataCluster(locationCluster));
	}

	//Now fileOffset was either set in findFreeFileLocation, or was set after extending cluster
	//chain

	//Load the file attributes and such into a struct including first free cluster number, and
	//then load contents as needed into successive clusters

	//First find first free data cluster (if necessary)
	unsigned int firstDataCluster = 0;
	if (fileSize > 0) {
		firstDataCluster = findFreeCluster();
	}

	File newFile(&currentDir, fileOffset, false, false, false, fileName, firstDataCluster, fileSize);
	writeFileDescription(fileOffset, newFile); //Write 32 byte file description

	if (fileSize > 0) {
		//then write the actual data into data clusters

		writeInData(inFile, fileSize, firstDataCluster);
	}

	currentDir = originalDir; //reset cwd
	cd(currentDir.path); //update directory information so that ls prints properly

	if (currentDir.fatOffset == rootDir.fatOffset) { //Special case of above for root directory
		loadRootDirectory();
		currentDir = rootDir;
	}

	return true;
}
