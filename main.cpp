#include <iostream>
#include <string>
#include <unistd.h>
using namespace std;

#include "fat.h"
#include "debug.h"

void runTerminal(unique_ptr<Fat>);

int main(int argc, char** argv) {
	if (argc != 2) {
		cerr << "Usage: fat [file_path]" << endl;
		return -1;
	}

	unique_ptr<Fat> pFat = Fat::open(argv[1]);

    if (pFat == nullptr) {
		cerr << "fail" << endl;
		return -1;
	}

	runTerminal(move(pFat));
}

void runTerminal(unique_ptr<Fat> pFat) {
	string line;
	while (true) {
		cout << ":" << pFat->getCurrentPath() << "> ";
		getline(cin, line);
		if (line == "quit" || line == "exit")
			break;

		pFat->processCommand(line);
	}
}
